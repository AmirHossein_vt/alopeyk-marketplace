import {Navigation} from 'react-native-navigation';
import {Platform, UIManager, I18nManager} from 'react-native';

import {registerScreens} from './src/screens';
import {registerElements} from './src/components/elements';
import {navigator} from './src/utils';

registerScreens();
registerElements();
if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}
I18nManager.allowRTL(false);

const App = () => {
    Navigation.events().registerAppLaunchedListener(() => {
        navigator.setDeafaults();
        navigator.setRoot('main');
    });
};

export default App;
