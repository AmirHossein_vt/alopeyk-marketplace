import React from 'react';
import {StyleSheet, Text} from 'react-native';
import PropTypes from 'prop-types';

import {THEME} from '../../constants';

const MText = ({type, children, style, light, dark, align}) => {
  return (
    <Text
      style={[s.defaultText, s[type], light && s.light, dark && s.dark, {textAlign: align}, style]}>
      {children}
    </Text>
  );
};

const s = StyleSheet.create({
  defaultText: {
    fontSize: 16,
    color: THEME.Color.second,
  },
  heading1: {
    color: THEME.Color.first,
    fontSize: 22,
  },
  heading2: {
    color: THEME.Color.first,
    fontSize: 20,
  },
  heading3: {
    color: THEME.Color.dark,
    fontSize: 18,
  },
  heading4: {
    color: THEME.Color.dark,
  },
  heading5: {
    color: THEME.Color.dark,
    fontSize: 14,
  },
  span: {
    color: THEME.Color.third,
    fontSize: 13,
  },
  mini: {
    color: THEME.Color.third,
    fontSize: 12,
  },
  paragraph1: {
    fontSize: 14,
  },
  paragraph2: {
    fontSize: 18,
  },
  light: {
    color: THEME.Color.light,
  },
  dark: {
    color: THEME.Color.first,
  },
});

MText.propTypes = {
  type: PropTypes.oneOf([
    'heading1',
    'heading2',
    'heading3',
    'heading4',
    'heading5',
    'span',
    'paragraph1',
    'paragraph2',
    'mini',
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  align: PropTypes.oneOf(['right', 'center', 'left', 'justify']),
};

MText.defaultProps = {
  align: 'right',
};

export {MText};
