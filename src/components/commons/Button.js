import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import PropTypes from 'prop-types';
import {Touchable} from './Touchable';
import {THEME} from '../../constants';
import {MText} from '.';
import {ShadowBox} from './ShadowBox';

const Button = ({
  disabled,
  loading,
  onPress,
  style,
  type,
  title,
  width,
  height,
  mainColor,
  titleType,
  textStyle = {},
}) => {
  let children = null;
  let customStyle = {};

  switch (type) {
    default:
    case 'primary':
      const color = disabled ? THEME.Color.second : mainColor || THEME.Color.primary;
      children = (
        <ShadowBox
          height={height}
          width={width}
          backgroundColor={color}
          radius={5}
          shadowColor={color}>
          <View style={[s.center, s.primaryStyle]}>
            {loading ? (
              <ActivityIndicator color={THEME.Color.light} size={'small'} />
            ) : (
              <React.Fragment>
                {title && (
                  <MText type={titleType} style={{... s.primaryTitle ,...textStyle}}>
                    {title}
                  </MText>
                )}
              </React.Fragment>
            )}
          </View>
        </ShadowBox>
      );
      break;
  }

  return (
    <Touchable
      disabled={disabled || loading}
      loading={loading}
      onPress={onPress}
      type="opacity"
      style={[customStyle, style]}>
      {children}
    </Touchable>
  );
};

const s = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  primaryStyle: {
    flex: 1,
  },
  primaryTitle: {
    marginHorizontal: 5,
    color: THEME.Color.light,
  },
});

Button.propTypes = {
  type: PropTypes.oneOf(['primary']),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  title: PropTypes.string,
  mainColor: PropTypes.string,
  titleType: PropTypes.string,
  onPress: PropTypes.func,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
};

Button.defaultProps = {
  style: {},
  type: 'primary',
  height: 45,
  width: 200,
  onPress: () => false,
  loading: false,
  disabled: false,
  titleType: 'heading4',
};

export {Button};
