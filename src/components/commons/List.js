import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, ActivityIndicator, Dimensions, RefreshControl} from 'react-native';
import {KeyboardAwareFlatList} from 'react-native-keyboard-aware-scroll-view';
import {RecyclerListView, DataProvider, LayoutProvider} from 'recyclerlistview';
import {MText} from './MText';
import {THEME} from '../../constants';

const {width: screenWidth} = Dimensions.get('window');

const List =
    ({
         style = {},
         containerStyle = {},
         data = [],
         store,
         loading = false,
         refresh = false,
         recycle = false,
         itemHeight = 0,
         headerHeight = 0,
         width = 0,
         renderItem = () => null,
         activeItem,
         ...props
     }) => {
        const scroller = useRef(null);
        const scrolled = useRef(0);
        const dataProvider = useRef(
            new DataProvider((r1, r2) => {
                return r1 !== r2;
            }),
        ).current;
        const headerData = [...(headerHeight ? ['HEADER'] : [])];
        const [refreshing, setRefresh] = useState(false);
        const [listDataProvider, setListDataProver] = useState(dataProvider.cloneWithRows(headerData));
        const listData = store ? store.list : data;
        const listLoading = store ? store.loading : loading;
        const listLastPage = listLoading ? false : store ? store.isLastPage : true;

        useEffect(() => {
            recycle && setListDataProver(listDataProvider.cloneWithRows([...headerData, ...listData]));
        }, [listData]);

        useEffect(() => {
            const scroll = scrolled.current;
            requestAnimationFrame(() => {
                scroller.current && scroller.current.scrollToOffset(0, scroll);
            });
        }, [listDataProvider, activeItem]);

        const ListEmptyComponent = () => {
            return listLastPage && !listData.length ? (
                <View style={s.emptyContainer}>
                    <MText>خالی !</MText>
                </View>
            ) : listLastPage ? null : (
                <View style={s.footerConatiner}>
                    <View style={s.loading}>
                        <ActivityIndicator size="small" color={THEME.Color.primary}/>
                    </View>
                </View>
            );
        };

        const onEndReached = () => {
            const {onEndReached: onEnd} = props;
            onEnd && onEnd();
            if (!listLastPage) {
                store && store.fetch();
            }
        };

        const onRefresh = () => {
            if (!refresh || !store) {
                return false;
            }
            setRefresh(true);
            doRefresh();
        };

        const doRefresh = () => {
            store.fetch(true).finally(() => {
                setRefresh(false);
            });
        };

        return recycle ? (
            !listData.length && !headerHeight ? (
                ListEmptyComponent()
            ) : (
                <RecyclerListView
                    layoutProvider={
                        new LayoutProvider(
                            (index) =>
                                headerHeight && index === 0
                                    ? 'HEADER'
                                    : activeItem + 1 === index
                                    ? 'ACTIVE'
                                    : 'MAIN',
                            (type, dim) => {
                                dim.width = width || screenWidth * 0.9;
                                if (type === 'HEADER') {
                                    dim.height = headerHeight;
                                } else if (type === 'ACTIVE') {
                                    dim.height = itemHeight + 100;
                                } else {
                                    dim.height = itemHeight;
                                }
                            },
                        )
                    }
                    extendedState={{activeItem}}
                    ref={scroller}
                    onScroll={({nativeEvent}) => {
                        const scroll = nativeEvent.contentOffset.y;
                        scrolled.current = scroll;
                    }}
                    dataProvider={listDataProvider}
                    rowRenderer={renderItem}
                    onEndReachedThreshold={0.5}
                    renderFooter={() => ListEmptyComponent()}
                    style={{...s.container, ...style}}
                    scrollViewProps={{
                        contentContainerStyle: [s.containerStyle, containerStyle],
                        showsHorizontalScrollIndicator: false,
                        refreshControl: <RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>,
                        ...props,
                    }}
                />
            )
        ) : (
            <KeyboardAwareFlatList
                removeClippedSubviews
                onEndReachedThreshold={0.5}
                showsHorizontalScrollIndicator={false}
                style={[s.container, style]}
                contentContainerStyle={[s.containerStyle, containerStyle]}
                keyExtractor={(item, index) => String(item.id || index)}
                data={listData}
                refreshing={refreshing}
                onRefresh={onRefresh}
                ListFooterComponent={() => ListEmptyComponent()}
                onEndReached={onEndReached}
                renderItem={renderItem}
                {...props}
            />
        );
    };


const s = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    containerStyle: {
        marginHorizontal: '5%',
        paddingBottom: 40,
        paddingTop: 5,
    },
    emptyContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: '90%',
        backgroundColor: THEME.Color.light,
        borderRadius: 10,
        marginTop: 20,
        padding: 10,
        paddingBottom: 20,
    },
    emptyImage: {
        width: screenWidth * 0.8,
        height: screenWidth * 0.66,
        resizeMode: 'contain',
    },
    footerConatiner: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
    },
    loading: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: THEME.Color.light,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export {List};
