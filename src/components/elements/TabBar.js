import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Dimensions, Animated, Easing} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {GLOBAL_CONST, THEME} from '../../constants';
import {MText, ShadowBox, Touchable} from '../commons';
const {tabs} = GLOBAL_CONST;
const {width} = Dimensions.get('window');
const centerSize = 65;

let toggleTabBar = (show) => [show];

const TabBar = () => {
    const [active, setActive] = useState(0);
    const tabAnim = useRef(new Animated.Value(1)).current;

    useEffect(() => {
        Navigation.mergeOptions('TAB_BAR', {
            bottomTabs: {
                currentTabIndex: active,
            },
        });
    }, [active]);

    toggleTabBar = (show) => {
        Animated.timing(tabAnim, {
            toValue: show ? 1 : 0,
            duration: 350,
            easing: Easing.out(Easing.cubic),
            useNativeDriver: false,
        }).start();
    };

    return (
        <Animated.View
            style={[
                s.wrapper,
                {
                    bottom: tabAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [-90, 0],
                    }),
                },
            ]}>
            <ShadowBox
                width={width}
                height={100}
                style={s.tabBar}
                radius={20}
                shadowY={0}
                shadowOpacity={0.03}
                shadowBlur={15}
                containerStyle={s.container}>
                {tabs.map((item, i) => (
                    <Touchable
                        type="opacity"
                        key={item.component}
                        style={s.tabWrapper}
                        onPress={() => {
                            setActive(i);
                        }}>
                        {
                            active === i &&
                            <View style={{marginTop:-3,width : 20 ,height: 7 , backgroundColor: THEME.Color.secondary,alignSelf:"center",borderRadius: 20}} />
                        }
                        <View style={[s.tab, active === i ? s.activeTab : {}]}>
                            <MText type="span" style={s.tabTitle}>
                                {item.tabTitle}
                            </MText>
                        </View>
                    </Touchable>
                ))}

            </ShadowBox>
        </Animated.View>
    );
};

const s = StyleSheet.create({
    wrapper: {
        height: 60,
        position: 'absolute',

    },
    tabBar: {
        position: 'absolute',
        bottom: 0,
    },
    container: {
        backgroundColor: 'white',
        flexDirection: 'row-reverse',
        justifyContent: 'space-around',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        overflow: 'visible',
        paddingBottom: 40,
    },
    tabWrapper: {
        flex: 1,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.5,
    },
    activeTab: {
        opacity: 1,
    },
    tabTitle: {
        color: THEME.Color.first,
        fontSize: 13,
        marginTop: 3,
    },
    stepHeader: {
        position: 'absolute',
        zIndex: 3,
        bottom: centerSize / 43,
        left: width / 2,
        marginLeft: -centerSize / 2,
        marginTop: -centerSize / 2,
    },
    stepHeaderCircle: {
        borderWidth: 5,
        borderRadius: centerSize / 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    stepHeaderIcon: {
        position: 'absolute',
        zIndex: 2,
    },
});

export {TabBar, toggleTabBar};
