import React from 'react';
import {View, StyleSheet, Image, Dimensions} from 'react-native';
import Svg, {Rect, Defs, LinearGradient, Stop} from 'react-native-svg';
import {MText} from '../commons/index';
import {THEME} from '../../constants/index';

const {width} = Dimensions.get('window');

const NavBar = ({navBack = false, title}) => {

  const renderCenterSide = () => {
    return (
      <View style={[s.side, s.centerSide]}>
        {title ? (
          <MText type="heading4" light>
            {title}
          </MText>
        ) : (
          <Image style={s.logo} source={require('../../assets/logo.jpg')} />
        )}
      </View>
    );
  };



  return (
    <View style={s.wrapper}>
        <View style={s.backgroundImage}>
            <Svg width={width} height={width * 0.6} preserveAspectRatio="none">
                <Defs>
                    <LinearGradient id="grad" x1={"40%"} y1="0%" x2="50%" y2="100%">
                        <Stop offset="0" stopColor={THEME.Color.primary} stopOpacity="1" />
                        <Stop offset="1" stopColor={THEME.Color.secondary} stopOpacity="1" />
                    </LinearGradient>
                </Defs>
                <Rect width={width} height={width * 0.6} fill="url(#grad)" />
            </Svg>
        </View>
      <View style={s.container}>
        {renderCenterSide()}
      </View>
    </View>
  );
};

const s = StyleSheet.create({
  wrapper: {
    height: 65,
    width: '100%',
    position: 'relative',
  },
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    position: 'relative',
    zIndex: 2,
    flexDirection: 'row-reverse',
    paddingHorizontal: 10,
  },
  backgroundImage: {
    width,
    height: width * 0.6,
    position: 'absolute',
    top: -1 * 40,
    zIndex: 1,
    overflow: 'hidden',
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },
  button: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    position: 'relative',
  },
  side: {
    flex: 1,
  },
  rightSide: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  leftSide: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  centerSide: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2,
  },
  logo: {
    width: 16,
    height: 16 * 1.75,
    resizeMode: 'contain',
  },
  unread: {
    width: 22,
    height: 22,
    backgroundColor: THEME.Color.danger,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 11,
    position: 'absolute',
    bottom: -2,
    right: -2,
    zIndex: 10,
  },
});

export {NavBar};
