import {Navigation} from 'react-native-navigation';
import {TabBar} from './TabBar';

const registerElements = () => {
    Navigation.registerComponent('TabBar', () => TabBar);
};

export {registerElements};
export * from './NavBar';
export * from './TabBar';
