import {LayoutAnimation, Platform} from 'react-native';

export const layoutAnimation = ()=>{
    const CustomLayoutSpring = {
        duration: Platform.select({ios: 600, android: 800}),
        create: {
            type: LayoutAnimation.Types.spring,
            property: LayoutAnimation.Properties.scaleXY,
            springDamping: Platform.select({ios: 0.7, android: 0.9}),
        },
        update: {
            type: LayoutAnimation.Types.spring,
            springDamping: Platform.select({ios: 0.7, android: 0.9}),
        },
    };
    LayoutAnimation.configureNext(CustomLayoutSpring);
};

export const getRandomJsonKey = (JSON)=>{
    const keys = Object.keys(JSON);
    const randIndex = Math.floor(Math.random() * keys.length);
    const randKey = keys[randIndex];
    return JSON[randKey];
};

