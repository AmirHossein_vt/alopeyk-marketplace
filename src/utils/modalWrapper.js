import React, {useEffect, useRef} from 'react';
import {View, StyleSheet, Animated, Easing, Dimensions} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {Touchable, MText, Button} from './../components/commons';
import {THEME} from './../constants';

const {width} = Dimensions.get('window');

const modalWrapper = (Component) => ({componentId, title, closeButton = true, ...props}) => {
  const drawerAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(drawerAnim, {
      toValue: 1,
      duration: 350,
      easing: Easing.out(Easing.cubic),
      useNativeDriver: true,
    }).start();
  }, []);

  const closeModal = () => {
    Animated.timing(drawerAnim, {
      toValue: 0,
      duration: 250,
      easing: Easing.inOut(Easing.cubic),
      useNativeDriver: true,
    }).start(() => Navigation.dismissOverlay(componentId));
  };
  return (
    <View style={s.container}>
      <Animated.View
        style={[
          s.background,
          {
            opacity: drawerAnim,
          },
        ]}>
        <Touchable type="opacity" style={s.flex} onPress={closeModal} />
      </Animated.View>
      <Animated.View
        style={[
          s.drawer,
          {
            transform: [
              {
                scale: drawerAnim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1.1, 1],
                }),
              },
            ],
            opacity: drawerAnim,
          },
        ]}>
        <View style={s.header}>
          <MText type="heading4">{title}</MText>
        </View>
        <Component {...props} closeModal={closeModal} />
        {closeButton ? (
          <Button type="primary" title="بستن" width={width * 0.9 - 40} onPress={closeModal} />
        ) : null}
      </Animated.View>
    </View>
  );
};

const s = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    zIndex: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flex: {
    flex: 1,
  },
  background: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    flex: 1,
    position: 'absolute',
    zIndex: 1,
    right: 0,
    top: 0,
    width: '100%',
    height: '100%',
  },
  drawer: {
    width: width * 0.9,
    backgroundColor: THEME.Color.white,
    position: 'relative',
    flexDirection: 'column',
    zIndex: 2,
    borderRadius: 5,
    paddingBottom: 20,
    alignItems: 'center',
  },
  header: {
    padding: 15,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: THEME.Color.secondBorder,
    alignItems: 'center',
  },
});

export {modalWrapper};
