import React from 'react';
import {View, StyleSheet, Keyboard, ActivityIndicator} from 'react-native';
import {Navigation} from 'react-native-navigation';

import {THEME, GLOBAL_CONST} from '../constants';
import {navigator} from './navigator';
import {NavBar} from './../components/elements';

const Container = ({children, paddingTop, paddingBottom, tab}) => {

    return (
        <View
            style={[
                s.container,
                paddingTop && s.paddingTop,
                paddingBottom && s.paddingBottom,
                tab && {paddingBottom:  40,},
            ]}>
            {children}
        </View>
    );
};

const pageWrapper = (
    Component,
    {
        paddingTop = true,
        paddingBottom = true,
        tab = false,
        nav = true,
        navBack = false,
        notification = true ,
        drawer = true,
        title = '',
    } = {},
) => {
    class Page extends React.Component {
        state = {
            loaded: false,
        };

        componentDidMount() {
            requestAnimationFrame(() => {
                this.setState({
                    loaded: true,
                });
            });
            this.navigationEventListener = Navigation.events().bindComponent(this);
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        }

        componentWillUnmount() {
            if (this.navigationEventListener) {
                this.navigationEventListener.remove();
            }
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
            navigator.showTabBar();
        }

        componentDidAppear() {
            navigator.setComponentId(this.props.componentId);
        }

        _keyboardDidShow() {
            navigator.hiddenTabBar();
        }

        _keyboardDidHide() {
            navigator.showTabBar();
        }

        render() {
            return (
                <Container {...{paddingTop, paddingBottom, tab}}>

                    {nav && <NavBar {...{navBack, title , drawer,notification}} {...this.props} />}
                    {this.state.loaded ? (
                        <View style={s.page}>
                            <Component {...this.props} />
                        </View>
                    ) : (
                        <View style={s.loading}>
                            <ActivityIndicator size="large" color={THEME.Color.primary} />
                        </View>
                    )}
                </Container>
            );
        }
    }
    return Page;
};

const s = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.Color.firstBg,
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    page: {
        flex: 1,
        position: 'relative',
        zIndex: 10,
    },
    paddingTop: {
        paddingTop: 40,
    },
    paddingBottom: {
        paddingBottom: 40,
    },
    tabPage: {
        paddingBottom: GLOBAL_CONST.tabPagesPadding + 40,
    }
});

export {pageWrapper};
