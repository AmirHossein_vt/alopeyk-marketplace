import {Navigation} from 'react-native-navigation';
import {Dimensions} from 'react-native';
import {GLOBAL_CONST} from '../constants';

const {width} = Dimensions.get('window');
const overlayOptions = {
  options: {
    overlay: {
      interceptTouchOutside: false,
    },
    layout: {
      backgroundColor: 'transparent',
      componentBackgroundColor: 'transparent',
    },
  },
};

class Navigator {
  noTabID = null;
  componentId = null;
  tabBarEnable = false;

  setComponentId = (id) => {
    this.componentId = id;
    if (this.noTabID && this.componentId === this.noTabID) {
      this.noTabID = null;
      this.showTabBar();
    }
  };

  showTabBar = () => {
    if (this.tabBarEnable || this.noTabID) {
      return;
    }
    this.tabBarEnable = true;
  };

  hiddenTabBar = () => {
    if (!this.tabBarEnable) {
      return;
    }
    this.tabBarEnable = false;
  };


  setTabBar = () => {
    this.tabBarEnable = true;
    Navigation.showOverlay({
      component: {
        id: 'TABBAR',
        name: 'TabBar',
        ...overlayOptions,
      },
    });
  };

  setRoot = (type) => {
    switch (type) {
      case 'main':
        this.setTabBar();
        Navigation.setRoot({
          root: {
            bottomTabs: {
              id: 'TAB_BAR',
              children: GLOBAL_CONST.tabs.map((item) => ({
                stack: {
                  children: [
                    {
                      component: {
                        id: item.component,
                        name: item.component,
                        options: {
                          bottomTabs: {
                            visible: false,
                          },
                        },
                      },
                    },
                  ],
                },
              })),
              options: {
                bottomTabs: {
                  visible: false,
                },
              },
            },
          },
        });
        break;
      default:
        Navigation.setRoot({
          root: {
            component: {
              name: 'Shop',
            },
          },
        });
    }
  };

  setDeafaults = () => {
    Navigation.setDefaultOptions({
      bottomTabs: {
        visible: false,
      },
      layout: {
        orientation: ['portrait'],
        direction: 'ltr',
      },
      topBar: {
        visible: false,
      },
      popGesture: true,
      animations: {
        setRoot: {
          alpha: {
            duration: 200,
            from: 0,
            to: 1,
          },
        },
        push: {
          content: {
            translationX: {
              from: width,
              to: 0,
              duration: 250,
            },
          },
        },
        pop: {
          content: {
            translationX: {
              from: 0,
              to: width,
              duration: 200,
            },
          },
        },
      },
    });
  };

  push = (screen, {props = {}} = {}, noTab = false) => {
    if (noTab && !this.noTabID) {
      this.noTabID = this.componentId;
      this.hiddenTabBar();
    }
    Navigation.push(this.componentId, {
      component: {
        name: screen,
        passProps: {navBack: true, ...props},
      },
    });
  };

  pop = () => {
    Navigation.pop(this.componentId);
  };




  setModal = (modal, {title, props = {}} = {}) => {
    Navigation.showOverlay({
      component: {
        name: modal,
        ...overlayOptions,
        passProps: {
          title,
          ...props,
        },
      },
    });
  };
}

export const navigator = new Navigator();
