const GLOBAL_CONST = {
    tabs: [
        {
            component: 'Shop',
            tabTitle: 'فروشگاه',
        },
        {
            component: 'History',
            tabTitle: 'سوابق',
        },

    ],
    tabPagesPadding: 60,
    categories : [
        {
            id : 1 ,
            title : 'آشامیدنی',
            products:[1,2,3,14,15,16]
        },
        {
            id : 2 ,
            title : 'پوشیدنی',
            products:[4,5,17,18,19]
        },
        {
            id : 3 ,
            title : 'لوازم خانه',
            products:[6,7,8]

        },
        {
            id : 4 ,
            title : 'لوازم ماشین',
            products:[9,10,11,12,13,20,21]
        },
        {
            id : 5 ,
            title : 'مصرفی',
            products:[14]

        }
    ],
    ordersStatus : [
        "sent",
        "delivered",
        "deleted",
        "processing",
        "sending",
        "waiting",
        "verifying",
        "missed"
    ]

};


export {GLOBAL_CONST}
