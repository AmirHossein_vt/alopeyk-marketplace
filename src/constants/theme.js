const THEME = {
    Color: {
        primary: '#2d3240',
        secondary: '#00bffe',
        first: '#313F41',
        second: '#6A7678',
        third: '#94A0A2',
        fourth: '#009A94',
        light: '#FFFFFF',
        dark: '#1C292B',
        firstBg: '#F8F9F9',
        secondBg: '#00df9a',
        danger: '#FF5656',
        warn: '#DEAB5E',
        success: '#00C286',
        firstBorder: '#DDEBEB',
        secondBorder: '#EBEBEB',
        inputBg: '#F4F7F8',
        white: '#fff',
    },
};

export {THEME};
