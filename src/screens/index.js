import {Navigation} from 'react-native-navigation';

import {History} from './History';
import {Shop} from './Shop';
import {PickLocation} from './Shop/components/PickLocation';


const registerScreens = () => {
    Navigation.registerComponent('History', () => History);
    Navigation.registerComponent('Shop', () => Shop);
    Navigation.registerComponent('Modal.PickLocation', () => PickLocation);
};

export {registerScreens};
