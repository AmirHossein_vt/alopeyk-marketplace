import React, {useState} from 'react';
import {StyleSheet, Dimensions, View} from 'react-native';
import {THEME} from './../../../../../src/constants';
import {MText,Button} from './../../../../../src/components/commons';
import { modalWrapper} from '../../../../utils/index';
import MapView from "react-native-maps";

const PickLocationPage = ({callback = () => false,closeModal}) => {

    const [region , setRegion] = useState({
        latitude: 35.6892,
        longitude: 51.3890,
        latitudeDelta: 0.001,
        longitudeDelta: 0.001
    });

    const onRegionChange = region => {
        setRegion(region);
    };
    const onLocationSelect = () => {
        callback(region);
        closeModal();
    };

    return  (
        <View style={styles.container}>
            <View style={styles.wrapper2}>
                <MapView
                    style={styles.map}
                    initialRegion={region}
                    showsUserLocation={true}
                    onRegionChangeComplete={onRegionChange}
                >
                </MapView>
                <View style={styles.mapMarkerContainer}>
                    <MText name={"apple"} style={styles.marker} >📍</MText>
                </View>
            </View>
            <View style={styles.pickButton}>
                <Button
                    title="انتخاب آدرس"
                    onPress={onLocationSelect}
                >
                </Button>
            </View>

        </View>
    );


};



const styles = StyleSheet.create({
    wrapper: {
        paddingTop: 40,
        paddingBottom: 25,
        justifyContent:"center",
        alignItems:"center"
    },
    wrapper2:{ flex: 2},
    marker:{ fontSize: 42, color: THEME.Color.warn },
    container: {
        display: "flex",
        height: "60%",
        width: "100%",
    },
    pickButton:{width:"100%",height: "50%",justifyContent:"center", alignItems:"center"},
    map: {
        flex: 1
    },
    mapMarkerContainer: {
        left: '47%',
        position: 'absolute',
        top: '42%'
    },
    mapMarker: {
        fontSize: 40,
        color: "red"
    },
    deatilSection: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 10,
        display: "flex",
        justifyContent: "center"
    },
    spinnerView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    btnContainer: {
        width: Dimensions.get("window").width - 20,
        position: "absolute",
        bottom: 100,
        left: 10
    }
});

const PickLocation = modalWrapper(PickLocationPage);

export {PickLocation};
