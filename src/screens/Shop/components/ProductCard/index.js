import React from 'react';
import {View, StyleSheet,Dimensions} from 'react-native';
import {THEME} from './../../../../../src/constants';
import {Button} from './../../../../components/commons';
import {navigator} from './../../../../utils';
const {width} = Dimensions.get('window');
import AsyncStorage from '@react-native-community/async-storage';

const ProductCard = ({id}) => {


    const doOrder = async (region)=>{
        let orders = JSON.parse(await AsyncStorage.getItem("orders")) || [];
        const orderId = new Date().getTime();
        AsyncStorage.setItem("orders",JSON.stringify([...orders , {region , productId : id , id : orderId , status :"sent" }]));
        navigator.push("History")
    };


    return (
        <View style={s.global}>
                            <Button
                                height={30}
                                mainColor={THEME.Color.secondBg}
                                type="primary"
                                textStyle={s.textStyle}
                                style={s.style}
                                title={'خرید محصول : '  + id}
                                width={width/4.5}
                                onPress={() => {
                                    navigator.setModal('Modal.PickLocation', {
                                        title: 'ثبت آدرس',
                                        props: {
                                            closeButton: false,
                                            callback: doOrder,
                                        },
                                    });
                                }}
                            />
        </View>
    );
};

const s = StyleSheet.create({
    style:{margin:10},
    textStyle:{fontSize:10},
    global:{justifyContent:"center",alignItems:"center"}

});

export {ProductCard};
