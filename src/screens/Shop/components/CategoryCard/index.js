import React from 'react';
import {View, StyleSheet, ScrollView, Platform} from 'react-native';
import {ProductCard} from './../../../../../src/screens/Shop/components/ProductCard';
import {THEME} from './../../../../constants';
import {MText, Touchable} from '../../../../components/commons';

const CategoryCard = ({
                      id,
                      title,
                      handleActive = () => false,
                      active,
                      products
                  }) => {

    const isActive = active === id;



    return (
        <View style={s.container}>
            <Touchable type="opacity" style={s.header} onPress={() => handleActive(id)}>
                <View style={s.headerRight}>
                    <View style={s.titleWrapper}>
                        <MText type="heading4">{title}</MText>
                    </View>
                </View>
            </Touchable>

            {isActive ? (
                <View style={s.footer}>
                    <ScrollView horizontal={true} >
                        <View style={s.products}>
                           {
                               products.map(product =>(<ProductCard key={product} id={product}/>))
                           }
                        </View>
                    </ScrollView>
                </View>
            ) : null}

        </View>
    );
};

const s = StyleSheet.create({
    products:{width:"100%",height:"100%",flexDirection:"row"},
    container: {
        flex: 1,
        backgroundColor: THEME.Color.white,
        marginTop: 15,
        borderWidth: 1,
        borderColor: THEME.Color.secondBorder,
        borderRadius: 10,

    },
    header: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        height: 80,
    },
    headerRight: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent:"center",
    },
    titleWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent:"center",
        alignItems:"center"
    },
    headerIcon: {
        width: 30,
        height: 30,
        resizeMode: 'cover',
        marginLeft: 10,
    },
    closeTop: {
        marginTop: Platform.select({
            android: -2,
            ios: 3,
        }),
    },
    headerLeft: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    star: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    marginCol: {
        marginLeft: 5,
        marginRight: 2,
    },
    headerPriceArrow: {
        margin: 1,
        transform: [
            {
                rotate: '90deg',
            },
        ],
    },
    arrowTop: {
        transform: [
            {
                rotate: '-90deg',
            },
        ],
    },
    activeArrow: {
        transform: [
            {
                rotate: '180deg',
            },
        ],
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'space-around',
        borderTopWidth: 1,
        borderTopColor: THEME.Color.firstBorder,
        flexDirection:"column",
        height:90
    },
    footerItem: {
        padding: 5,
        borderRadius: 5,
        marginLeft:10,
        backgroundColor: THEME.Color.firstBorder,
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    footerItemPercent: {
        marginRight: 5,
    },
    footerItemArrow: {
        transform: [
            {
                rotate: '90deg',
            },
        ],
    },
});

export {CategoryCard};
