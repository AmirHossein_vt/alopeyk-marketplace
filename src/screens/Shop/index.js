import React, {useState,useEffect} from 'react';
import {GLOBAL_CONST} from './../../../src/constants';
import {List} from './../../../src/components/commons';
import {CategoryCard} from './../../../src/screens/Shop/components/CategoryCard';
import {pageWrapper,layoutAnimation} from './../../utils';


const ShopPage = () => {
    const [active, setActive] = useState(null);
    const [categories, setCategories] = useState([]);

    const handleActive = (id) => {
        setActive(active === id ? null : id);
        layoutAnimation();
    };

    useEffect(()=>{
        setTimeout(()=>{ //simulate api fetch timeout
            setCategories(GLOBAL_CONST.categories);
        },1000);
    },[]);

    return(
        <List
            recycle={true}
            itemHeight={97}
            headerHeight={50}
            data={categories}
            activeItem={categories.findIndex((item) => item.id === active)}
            loading={categories.length === 0}
            renderItem={(type, item) =>
                (
                    type !== 'HEADER' && <CategoryCard {...item} handleActive={handleActive} active={active} />
                )
            }
        />
    );
};



const Shop = pageWrapper(ShopPage, {
    tab: true,
    title: 'محصولات',
    navBack: true,
});

export {Shop};
