import React,{useState} from 'react';
import {GLOBAL_CONST} from './../../../src/constants';
import {getRandomJsonKey} from './../../../src/utils';
import {OrderCard} from './../../../src/screens/History/components/OrderCard';
import {List} from './../../../src/components/commons';
import {pageWrapper} from './../../utils';
import {useNavigationComponentDidAppear , useNavigationComponentDidDisappear} from 'react-native-navigation-hooks';
import AsyncStorage from '@react-native-community/async-storage';

const HistoryPage = ({componentId}) => {
    const [orders , setOrders] = useState([]);
    const [intervals , setIntervals] = useState([]);

    useNavigationComponentDidAppear(() => {
        startLoadAndChangeIntervals();
    }, componentId);

    useNavigationComponentDidDisappear(() =>{
        destroyLoadAndChangeIntervals();
    },componentId);


    const startLoadAndChangeIntervals = ()=>{
        const loadInterval = setInterval( ()=>{
            loadOrders();
        },5000);

       const changeIntervals = setInterval( ()=>{
            changeOrdersStatus();
        },30000);
       setIntervals([loadInterval,changeIntervals]);
    };

    const destroyLoadAndChangeIntervals = ()=>{
        clearInterval(intervals[0]);
        clearInterval(intervals[1]);
    };


    const loadOrders = async ()=>{
        const orders = JSON.parse(await AsyncStorage.getItem("orders"));
        setOrders(orders)
    };

    const changeOrdersStatus = async ()=>{
        const orders = JSON.parse(await AsyncStorage.getItem("orders")) || [];
        let freshOrders = [];
        orders.forEach((order)=>{
            const nextStatus = getRandomJsonKey(GLOBAL_CONST.ordersStatus);
            order.status = nextStatus;
            freshOrders.push(order);
        });
        AsyncStorage.setItem("orders",JSON.stringify(freshOrders))
    };



    return(
        <List
            recycle={true}
            itemHeight={97}
            headerHeight={50}
            data={orders}
            loading={orders.length === 0}
            renderItem={(type, item) =>
                (
                    type !== 'HEADER' && <OrderCard {...item} />
                )
            }
        />
    );
};



const History = pageWrapper(HistoryPage, {
    tab: true,
    title: 'سوابق خرید',
    navBack: false,
});

export {History};
