import React from 'react';
import {View, StyleSheet} from 'react-native';
import MapView from "react-native-maps";
import {THEME} from './../../../../../src/constants';
import {MText, Touchable} from '../../../../components/commons';

const OrderCard = ({
                          id,
                          status = "unValid",
                            region
                      }) => {

    return (
        <View style={s.container}>
            <Touchable type="opacity" style={s.header}>
                <View style={s.headerRight}>
                    <View style={s.titleWrapper}>
                        <MText type="heading4">{status}</MText>
                    </View>
                    <View style={s.titleWrapper}>
                        <MapView
                            style={s.map}
                            initialRegion={region}
                            showsUserLocation={true}
                        >
                             <MapView.Marker
                                coordinate={{ "latitude": region.latitude, "longitude": region.longitude }}
                                title={"user Location"}
                                />
                        </MapView>
                    </View>
                    <View style={s.factor}>
                        <MText  type="mini">{"factor : "+id}</MText>
                    </View>
                </View>
            </Touchable>
        </View>
    );
};

const s = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.Color.white,
        marginTop: 15,
        borderWidth: 1,
        borderColor: THEME.Color.secondBorder,
        borderRadius: 10,

    },
    factor:{margin:10},
    map:{  flex: 1, marginTop: 0 },
    headerLeft: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    header: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        height: 80,
    },
    headerRight: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent:"center",
    },
    titleWrapper: {
        flex: 1,
        flexDirection: 'column',

    },
});

export {OrderCard};
